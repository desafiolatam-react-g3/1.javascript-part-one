INSTRUCCIONES
---

### Instalar dependencias
```
npm install
```

### Correr scripts
```
npm run babel 1.ecmascript.js

npm run babel 3.objects.js
```

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados


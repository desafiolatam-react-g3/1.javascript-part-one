// Existen varias formas de escuchar y manejar eventos
window.onload = function () {
  var boton1 = document.getElementById('boton-1');
  var boton2 = document.getElementById('boton-2');

  // Asignar un manejador directamente a la propiedad
  // onclick del element
  boton1.onclick = manejador;

  // Agregar un escuchador al evento click y asignarle
  // de esta forma un manejador
  boton2.addEventListener('click', manejador);
};

function manejador(event) {
  console.log(event.target.id);
  // boton-1
  // boton-2
  // boton-3
}

console.log('=================================================================');
console.log('Literales\n');
// ==============================================================
const string = 'Hola, soy un literal';
const number = 1234;
const array = [1, '2', 3, '4'];

console.log('string: ', string);
// output: string: Hola, soy un literal
console.log('number: ', number);
// output: number: 1234
console.log('array: ', array);
// output: array: [1, '2', 3, '4']
// ==============================================================
console.log('=================================================================');
console.log('Objetos literales\n');
// ==============================================================
const object = {
  param1: 1, // números
  param2: 'string', // cadenas de texto
  param3: [1, '2', 3, '4'], // arreglos
  param4: {}, // objetos literales
  param5() {}, // funciones declarativas
  param6: function () {}, // funciones expresivas
  param7: () => null, // functiones de flecha gorda
};

console.log('object: ', object);
// output: object: {
//    param1: 1,
//    param2: 'string',
//    param3: [ 1, '2', 3, '4' ],
//    param4: {},
//    param5: [Function: param5],
//    param6: [Function: param6],
//    param7: [Function: param7]
// }
// ==============================================================
console.log('=================================================================');
console.log('Acceder a parámetros\n');
// ==============================================================
console.log('object.param1: ', object.param1);
// output: object.param1: 1
console.log('object["param1"]: ', object['param1']);
// output: object["param1"]: 1
// ==============================================================
console.log('=================================================================');
console.log('Cambiar parámetros y definir nuevos\n');
// ==============================================================
object.param1 = 1234;
object['param2'] = 'cambio de string';
object.param8 = 8888;
object['param9'] = 'nuevo parámetro';

console.log('object.param1: ', object.param1);
// output: object.param1: 1234
console.log('object["param2"]: ', object.param2);
// output: object["param2"]: cambio de string
console.log('object.param8: ', object.param8);
// output: object.param8: 8888
console.log('object["param9"]: ', object.param9);
// output: object["param9"]: nuevo parámetro
// ==============================================================
console.log('=================================================================');
console.log('Uso de objetos literales\n');
// ==============================================================
const profile = {
  firstName: 'sebastian',
  lastName: 'rodriguez',
  age: 32,
  sayHi1: () => `Hola, soy ${this.firstName} ${this.lastName}`,
  sayHi2: () => `Hola, soy ${profile.firstName} ${profile.lastName}`, // ¿Por qué?
  sayHi3() {
    return `Hola, soy ${this.firstName} ${this.lastName}`;
  },
  sayHi4: function() {
    return `Hola, soy ${this.firstName} ${this.lastName}`;
  },
};

// console.log('profile.sayHi1(): ', profile.sayHi1());
// output: TypeError: Cannot read property 'firstName' of undefined // ¿Por qué el error?
console.log('profile.sayHi2(): ', profile.sayHi2());
// output: profile.sayHi2(): Hola, soy sebastian rodriguez
console.log('profile.sayHi3(): ', profile.sayHi3());
// output: profile.sayHi3(): Hola, soy sebastian rodriguez
console.log('profile.sayHi4(): ', profile.sayHi4());
// output: profile.sayHi4(): Hola, soy sebastian rodriguez
// ==============================================================
// ==============================================================
// Objetos literales en funciones
const returnObj = () => {
  return {
    a: 1,
    b: 2,
    c: 3,
  };
};

console.log('returnObj(): ', returnObj());
// output: returnObj(): { a: 1, b: 2, c: 3 }
// ==============================================================

console.log('=================================================================');
console.log('Definición y uso de funciones\n');
// ==============================================================
function declarative() {
  return 'Hola mundo con una función declarativa';
}

var expressive = function () {
  return 'Hola mundo con una función expresiva';
};

console.log(declarative());
// output: hola mundo con una función declarativa
console.log(expressive());
// output: hola mundo con una función expresiva
// ==============================================================
// ==============================================================
console.log(defineDeclarativeAfterCall());
// output: hola mundo con una función declarativa

// No importa si al llamar a una función declarativa, esta aun no esta definida
function defineDeclarativeAfterCall() {
  return 'Hola mundo con una función declarativa';
}
// ==============================================================
// ==============================================================
// console.log(defineExpressiveAfterCall());
// output: TypeError: defineExpressiveAfterCall is not a function

// LLamar a función expresiva antes de definirla, nos da un error
var defineExpressiveAfterCall = function () {
  return 'Hola mundo con una función expresiva';
};
// ==============================================================
// ==============================================================
const fatArrowOneLine = () => 'Hola mundo con una función de flecha gorda en una línea';
const fatArrowWithLogic = () => {
  const message = 'Hola mundo con una función de flecha gorda con lógica';

  return { message: message };
};
const fatArrowWithoutLogic = () => ({
  message: 'Hola mundo con una función de flecha gorda sin lógica',
});

console.log(fatArrowOneLine());
// output: hola mundo con una función de flecha gorda en una línea
console.log(fatArrowWithLogic());
// output: { message: 'hola mundo con una función de flecha gorda con lógica' }
console.log(fatArrowWithoutLogic());
// output: { message: 'hola mundo con una función de flecha gorda sin lógica' }
// ==============================================================
console.log('=================================================================');
console.log('Concatenación de cadenas\n');
// ==============================================================
const firstName = 'sebastian';
const lastName = 'rodriguez';

console.log(firstName + lastName);
console.log(`${firstName} ${lastName}`);
// output: sebastian rodriguez
// ==============================================================
// ==============================================================
// Decora el nombre dado dejándolo en mayúsculas
function decorateName(name) {
  return name.toUpperCase();
}

console.log(decorateName(firstName) + decorateName(lastName));
// output: SEBASTIAN RODRIGUEZ
console.log(`${decorateName(firstName)} ${decorateName(lastName)}`);
// output: SEBASTIAN RODRIGUEZ
console.log(decorateName(firstName + lastName));
// output: SEBASTIAN RODRIGUEZ
console.log(decorateName(`${firstName} ${lastName}`));
// output: SEBASTIAN RODRIGUEZ
// ==============================================================
console.log('=================================================================');
console.log('Clave valor en objetos literales\n');
// ==============================================================
const profileES5 = {
  firstName: firstName,
  lastName: lastName,
};

const profileES2015 = {
  firstName,
  lastName,
};

console.log(profileES5);
// output: { firstName: 'sebastian', lastName: 'rodriguez' }
console.log(profileES2015);
// output: { firstName: 'sebastian', lastName: 'rodriguez' }
// ==============================================================
console.log('=================================================================');
console.log('Argumentos por defecto en funciones\n');
// ==============================================================
function defaultArgumentsInES5(arg1, arg2, arg3, arg4) {
  return {
    param1: arg1 || 1,
    param2: arg2 || 1,
    param3: arg3 || 1,
    param4: arg4 || 1,
  };
}

function defaultArgumentsInES2015(arg1 = 1, arg2 = 2, arg3 = 3, arg4 = 4) {
  return {
    param1: arg1,
    param2: arg2,
    param3: arg3,
    param4: arg4,
  };
}

console.log(defaultArgumentsInES5(1, 2, 3, 4));
// output: { param1: 1, param2: 2, param3: 3, param4: 4 }
console.log(defaultArgumentsInES5(1, 2));
// output: { param1: 1, param2: 2, param3: 3, param4: 4 }
console.log(defaultArgumentsInES2015(1, 2, 3, 4));
// output: { param1: 1, param2: 2, param3: 3, param4: 4 }
console.log(defaultArgumentsInES2015(1, 2));
// output: { param1: 1, param2: 2, param3: 3, param4: 4 }
// ==============================================================
console.log('=================================================================');
console.log('Leer multiples argumentos en funciones\n');
// ==============================================================
function getArgumentsInES5() {
  return arguments;
}

function getArgumentsInES2015(...args) {
  return args;
}

console.log(getArgumentsInES5(1, 2, 3, 4, 5));
// output: { '0': 1, '1': 2, '2': 3, '3': 4, '4': 5 }
console.log(getArgumentsInES2015(1, 2, 3, 4, 5));
// output: [ 1, 2, 3, 4, 5 ]
// ==============================================================
